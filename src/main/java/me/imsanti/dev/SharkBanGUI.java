package me.imsanti.dev;

import me.imsanti.dev.listeners.InventoryClickListener;
import me.imsanti.dev.listeners.JoinListener;
import me.imsanti.dev.listeners.PlayerCommandEvent;
import me.imsanti.dev.managers.gui.ConfigFile;
import me.imsanti.dev.managers.ConfigManager;
import me.imsanti.dev.managers.MessagesF;
import me.imsanti.dev.managers.gui.MuteFile;
import me.imsanti.dev.managers.gui.WarnFile;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

public final class SharkBanGUI extends JavaPlugin {

    private final ConfigManager configManager;
    private final MuteFile muteFile;
    private final WarnFile warnFile;
    private final ConfigFile configFile;
    private final MessagesF messagesF;

    public SharkBanGUI() {
        this.configManager = new ConfigManager(this);
        this.configFile = new ConfigFile(configManager, this);
        this.muteFile = new MuteFile(configManager, this);
        this.warnFile = new WarnFile(configManager, this);
        this.messagesF = new MessagesF(configManager, this);

    }
    @Override
    public void onEnable() {
        createConfig();
        registerEvents();
    }

    @Override
    public void onDisable() {
        HandlerList.unregisterAll();

    }

    private void createConfig() {
        if(configFile.createConfigFile()) {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Created bangui.yml");
        }else {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Loaded bangui.yml");


        }

        if(muteFile.createConfigFile()) {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Created mutegui.yml");
        }else {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Loaded mutegui.yml");
        }

        if(warnFile.createConfigFile()) {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Created warngui.yml");
        }else {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Loaded warngui.yml");
        }

        if(messagesF.createConfigFile()) {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Created messages.yml");
        }else {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Loaded messages.yml");
        }
    }

    private void registerEvents() {
        getServer().getPluginManager().registerEvents(new PlayerCommandEvent(configFile, messagesF, muteFile, warnFile), this);
        getServer().getPluginManager().registerEvents(new InventoryClickListener(), this);
        getServer().getPluginManager().registerEvents(new JoinListener(), this);
    }
}
