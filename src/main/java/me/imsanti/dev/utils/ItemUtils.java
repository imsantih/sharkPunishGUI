package me.imsanti.dev.utils;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class ItemUtils {

    public static ItemStack createItem(ConfigurationSection section, Player target) {
        ItemStack item;
        if(section.getString("data_value") != null) {
            item = new ItemStack(Material.valueOf(section.getString("material").toUpperCase()), 1, Short.parseShort(section.getString("data_value")));
        }else {
            item = new ItemStack(Material.valueOf(section.getString("material").toUpperCase()));
        }

        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(ColorUtils.color(section.getString("name")).replace("%player%", target.getName()));
        List<String> lore = new ArrayList<>();
        for(String string : section.getStringList("lore")) {
            lore.add(ColorUtils.color(string).replace("%player%", target.getName()));
        }

        meta.setLore(lore);
        item.setItemMeta(meta);
        return item;
    }
}
