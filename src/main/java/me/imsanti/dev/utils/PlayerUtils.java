package me.imsanti.dev.utils;

import org.bukkit.Bukkit;

public class PlayerUtils {

    public static boolean isValidPlayer(String playerName) {
        if(Bukkit.getPlayer(playerName) == null || !Bukkit.getPlayer(playerName).isOnline()) return false;

        return true;
    }
}
