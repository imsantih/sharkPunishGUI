package me.imsanti.dev.managers.gui;

import me.imsanti.dev.SharkBanGUI;
import me.imsanti.dev.managers.ConfigManager;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;

public class WarnFile {

    private final SharkBanGUI sharkBanGUI;

    public WarnFile(ConfigManager configManager, SharkBanGUI sharkBanGUI) {
        this.configManager = configManager;
        this.sharkBanGUI = sharkBanGUI;
    }

    private final ConfigManager configManager;

    public boolean createConfigFile() {
        if(configManager.createConfigFile(String.valueOf(sharkBanGUI.getDataFolder()), "warngui")) return true;

        return false;
    }

    public void saveConfigFile(FileConfiguration config) {
        configManager.saveConfigFile(getConfigFile(), config);
    }

    public FileConfiguration getConfig() {
        return configManager.getConfigFromFile(String.valueOf(sharkBanGUI.getDataFolder()), "warngui");
    }

    public File getConfigFile() {
        return new File(sharkBanGUI.getDataFolder(), "warngui.yml");
    }
}