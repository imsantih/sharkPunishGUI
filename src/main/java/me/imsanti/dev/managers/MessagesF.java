package me.imsanti.dev.managers;

import me.imsanti.dev.SharkBanGUI;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;

public class MessagesF {

    private final SharkBanGUI sharkBanGUI;

    public MessagesF(ConfigManager configManager, SharkBanGUI sharkBanGUI) {
        this.configManager = configManager;
        this.sharkBanGUI = sharkBanGUI;
    }

    private final ConfigManager configManager;

    public boolean createConfigFile() {
        if(configManager.createConfigFile(String.valueOf(sharkBanGUI.getDataFolder()), "messages")) return true;

        return false;
    }

    public void saveConfigFile(FileConfiguration config) {
        configManager.saveConfigFile(getConfigFile(), config);
    }

    public FileConfiguration getConfig() {
        return configManager.getConfigFromFile(String.valueOf(sharkBanGUI.getDataFolder()), "messages");
    }

    public File getConfigFile() {
        return new File(sharkBanGUI.getDataFolder(), "messages.yml");
    }
}