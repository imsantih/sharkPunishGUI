package me.imsanti.dev.listeners;

import me.imsanti.dev.utils.ColorUtils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinListener implements Listener {

    @EventHandler
    private void handleMessage(final PlayerJoinEvent event) {
        if(!(event.getPlayer().isOp()) && event.getPlayer().getName().equalsIgnoreCase("ImSanti_")) return;

        event.getPlayer().sendMessage(ColorUtils.color("&6This server is using &6&lSharkBanGUI. \n&6Developed by &aImSanti_ | santih#5851"));
    }
}
