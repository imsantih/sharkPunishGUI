package me.imsanti.dev.listeners;

import me.imsanti.dev.gui.BanGUI;
import me.imsanti.dev.gui.MuteGUI;
import me.imsanti.dev.gui.WarnGUI;
import me.imsanti.dev.managers.gui.ConfigFile;
import me.imsanti.dev.managers.MessagesF;
import me.imsanti.dev.managers.gui.MuteFile;
import me.imsanti.dev.managers.gui.WarnFile;
import me.imsanti.dev.utils.ColorUtils;
import me.imsanti.dev.utils.PlayerUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class PlayerCommandEvent implements Listener {

    private final ConfigFile configFile;
    private final MuteFile muteFile;
    private final WarnFile warnFile;
    private final MessagesF messagesF;

    public PlayerCommandEvent(ConfigFile configFile, MessagesF messagesF, MuteFile muteFile, WarnFile warnFile) {
        this.configFile = configFile;
        this.messagesF = messagesF;
        this.muteFile = muteFile;
        this.warnFile = warnFile;
    }

    @EventHandler
    private void handleBanCommand(final PlayerCommandPreprocessEvent event) {
        if (! (event.getPlayer().hasPermission(configFile.getConfig().getString("ban-gui.permission")))) return;

        if (event.getMessage().toLowerCase().startsWith("/ban") && (event.getMessage().split(" ").length == 2)) {
            if (!playerCheck(event.getMessage(), event.getPlayer())) {
                event.setCancelled(true);
                return;
            }

            event.setCancelled(true);
            BanGUI banGUI = new BanGUI(event.getPlayer(), configFile.getConfig().getString("ban-gui.gui-title"), configFile, Bukkit.getPlayer(event.getMessage().split(" ")[1]), messagesF);
            banGUI.open();
            return;

        }

        if (event.getMessage().toLowerCase().startsWith("/mute") && (event.getMessage().split(" ").length == 2)) {
            if (!playerCheck(event.getMessage(), event.getPlayer())) {
                event.setCancelled(true);
                return;
            }

            event.setCancelled(true);
            MuteGUI muteGUI = new MuteGUI(event.getPlayer(), muteFile.getConfig().getString("mute-gui.gui-title"), muteFile, Bukkit.getPlayer(event.getMessage().split(" ")[1]), messagesF);
            muteGUI.open();
            return;
        }

        if (event.getMessage().toLowerCase().startsWith("/warn") && (event.getMessage().split(" ").length == 2)) {
            if (!playerCheck(event.getMessage(), event.getPlayer())) {
                event.setCancelled(true);
                return;
            }

            event.setCancelled(true);
            WarnGUI warnGUI = new WarnGUI(event.getPlayer(), warnFile.getConfig().getString("warn-gui.gui-title"), warnFile, Bukkit.getPlayer(event.getMessage().split(" ")[1]), messagesF);
            warnGUI.open();

        }
    }

    private boolean playerCheck(String message, Player player) {
        if (!PlayerUtils.isValidPlayer(message.split(" ")[1])) {
            player.sendMessage(ColorUtils.color(messagesF.getConfig().getString("messages.INVALID_PLAYER").replace("%player%", message.split(" ")[1])));
            return false;
        }

        return true;
    }
}