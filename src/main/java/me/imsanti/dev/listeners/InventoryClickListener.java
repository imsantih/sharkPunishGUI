package me.imsanti.dev.listeners;

import me.imsanti.dev.gui.GUIManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.io.IOException;
import java.util.UUID;

public class InventoryClickListener implements Listener {

    public InventoryClickListener() {
    }
    @EventHandler
    private void handleGUIButtons(final InventoryClickEvent event) throws IOException {
        if (!(event.getWhoClicked() instanceof Player)) return;
        Player player = (Player) event.getWhoClicked();
        UUID playerUUID = player.getUniqueId();

        UUID inventoryUUID = GUIManager.openInventories.get(playerUUID);
        if (inventoryUUID == null) return;

        event.setCancelled(true);
        GUIManager gui = GUIManager.getInventoriesByUUID().get(inventoryUUID);
        GUIManager.setupGUIAction action = gui.getActions().get(event.getSlot());
        if (action == null) return;

        action.click(player);

    }

    @EventHandler
    private void onInventoryClose(final InventoryCloseEvent event) {
        GUIManager.openInventories.remove(event.getPlayer().getUniqueId());

    }

    @EventHandler
    private void handleQuit(final PlayerQuitEvent event) {
        GUIManager.openInventories.remove(event.getPlayer().getUniqueId());
    }

}