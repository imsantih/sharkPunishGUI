package me.imsanti.dev.gui;

import me.imsanti.dev.managers.gui.ConfigFile;
import me.imsanti.dev.managers.MessagesF;
import me.imsanti.dev.utils.ColorUtils;
import me.imsanti.dev.utils.ItemUtils;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class BanGUI extends GUIManager {

    private final Player player;
    private final Player target;
    private  final ConfigFile configFile;
    private final MessagesF messagesF;

    public BanGUI(Player player, String title, ConfigFile configFile, Player target, MessagesF messagesF) {
        super(configFile.getConfig().getInt("ban-gui.gui-size"), ColorUtils.color(title).replace("%player%", target.getName()));


        this.target = target;
        this.player = player;
        this.configFile = configFile;
        this.messagesF = messagesF;
    }


    public void open() {
        refill(this);
        this.openGUI(player);
    }

    private  void refill(BanGUI banGUI) {
        ConfigurationSection section = configFile.getConfig().getConfigurationSection("ban-gui.items");
        for(String key : section.getKeys(false)) {
            ConfigurationSection insideSection = section.getConfigurationSection(key);
            ConfigurationSection timesSection = section.getConfigurationSection(key + ".times");


            if(player.hasPermission(insideSection.getString("permission"))) {
                banGUI.setItem(insideSection.getInt("slot"), ItemUtils.createItem(insideSection, target), player -> {

                    TimesGUI gui = new TimesGUI(player, ColorUtils.color( insideSection.getString("name")), timesSection, target, messagesF);
                    player.closeInventory();
                    gui.open();

                });
            }else {
                banGUI.setItem(insideSection.getInt("slot"), ItemUtils.createItem(insideSection, target), player -> {

                    player.sendMessage(ColorUtils.color(messagesF.getConfig().getString("messages.NO_PERMISSIONS")));
                    player.closeInventory();

                });
            }
        }
    }
}