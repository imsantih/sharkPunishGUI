package me.imsanti.dev.gui;

import me.imsanti.dev.managers.MessagesF;
import me.imsanti.dev.utils.ColorUtils;
import me.imsanti.dev.utils.ItemUtils;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class TimesGUI extends GUIManager {

    private final Player player;
    private  final Player target;

    private  final MessagesF messagesF;

    private final ConfigurationSection section;
    public TimesGUI(Player player, String title, ConfigurationSection section, Player target, MessagesF messagesF) {
        super(section.getInt("slots"), ColorUtils.color(title).replaceAll("%player%", target.getName()));


        this.player = player;
        this.section = section;
        this.target = target;
        this.messagesF = messagesF;
    }


    public void open() {
        refill(this);
        this.openGUI(player);
    }

    private void refill(TimesGUI timesGUI) {
        for(String key : section.getKeys(false)) {
            ConfigurationSection itemSection = section.getConfigurationSection(key);
            if(itemSection == null) continue;

            if(player.hasPermission(itemSection.getString("permission"))) {
                timesGUI.setItem(itemSection.getInt("slot"), ItemUtils.createItem(itemSection, target), player -> {
                    player.closeInventory();
                    player.performCommand(itemSection.getString("command").replace("%player%", target.getName()));
                });
            }else {
                timesGUI.setItem(itemSection.getInt("slot"), ItemUtils.createItem(itemSection, target), player -> {
                    player.closeInventory();
                    player.sendMessage(ColorUtils.color(messagesF.getConfig().getString("messages.NO_PERMISSIONS")));
                });
            }
        }
    }
}

