package me.imsanti.dev.gui;

import me.imsanti.dev.managers.MessagesF;
import me.imsanti.dev.managers.gui.MuteFile;
import me.imsanti.dev.utils.ColorUtils;
import me.imsanti.dev.utils.ItemUtils;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class MuteGUI extends GUIManager {

    private final Player player;
    private final Player target;
    private final MuteFile muteFile;
    private final MessagesF messagesF;

    public MuteGUI(Player player, String title, MuteFile muteFile, Player target, MessagesF messagesF) {
        super(muteFile.getConfig().getInt("mute-gui.gui-size"), ColorUtils.color(title).replace("%player%", target.getName()));


        this.target = target;
        this.player = player;
        this.muteFile = muteFile;
        this.messagesF = messagesF;
    }


    public void open() {
        refill(this);
        this.openGUI(player);
    }

    private  void refill(MuteGUI banGUI) {
        ConfigurationSection section = muteFile.getConfig().getConfigurationSection("mute-gui.items");
        for(String key : section.getKeys(false)) {
            ConfigurationSection insideSection = section.getConfigurationSection(key);
            ConfigurationSection timesSection = section.getConfigurationSection(key + ".times");


            if(player.hasPermission(insideSection.getString("permission"))) {
                banGUI.setItem(insideSection.getInt("slot"), ItemUtils.createItem(insideSection, target), player -> {

                    TimesGUI gui = new TimesGUI(player, ColorUtils.color( insideSection.getString("name")), timesSection, target, messagesF);
                    player.closeInventory();
                    gui.open();

                });
            }else {
                banGUI.setItem(insideSection.getInt("slot"), ItemUtils.createItem(insideSection, target), player -> {

                    player.sendMessage(ColorUtils.color(messagesF.getConfig().getString("messages.NO_PERMISSIONS")));
                    player.closeInventory();

                });
            }
        }
    }
}